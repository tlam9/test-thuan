variable "aws_region" {

  description = "Region of aws deployment"
  type        = string
  default     = "us-east-1"

}

variable "aws_access_key" {

  description = "AWS Access key for account"
  type        = string

}

variable "aws_secret_key" {

  description = "AWS Secret key for account"
  type        = string

}

variable "aws_token_session" {

  description = "AWS Session Token"
  type        = string

}

variable "ssh_key_name" {

  description = "Name of SSH Key"

}

variable "public_ssh_key" {

  description = "Public ssh key to add on aws account"

}

variable "sg_name" {

  description = "Name of SG"

}

variable "sg_ingress_protocol" {

  description = "Protocole pour les règles d'entrée du groupe de sécurité"

}

variable "sg_ingress_from_port" {

  description = "Port début pour SG"

}

variable "sg_ingress_to_port" {

  description = "Port de fin pour SG"

}

variable "sg_ingress_cidr_blocks" {

  description = "Blocs CDIR autorisés au SG"

}

variable "sg_egress_protocol" {

  description = "Règles de sortie pour SG"

}

variable "sg_egress_from_port" {

  description = "Port début pour SG sur egress"

}

variable "sg_egress_to_port" {

  description = "Port de fin pour SG"

}

variable "sg_egress_cidr_blocks" {

  description = "Blocs CDIR autorisés au SG sur egresss"

}

variable "ec2_ami" {

  description = "ID de l'AMI pour l'instance EC2"

}

variable "ec2_instance_type" {

  description = "Type d'instance pour EC2"

}

variable "ec2_name" {

  description = "Nom de l'instance"

}